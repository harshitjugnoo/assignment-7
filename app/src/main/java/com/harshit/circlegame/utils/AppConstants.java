package com.harshit.circlegame.utils;

/**
 * Created by Harshit on 2/10/2015.
 */
public interface AppConstants {
    String INSIDE = "Inside the circle";
    String OUTSIDE = "Outside the circle";
    //    String CLICK = "Click Somewhere";
    String ON = "On the Circle";
    String GREEN = "Green";
    String RED = "Red";
    String INSIDE_CIRCLE_X = "Inside Circle ";
    String CIRCLE_1 = "Inside circle 1";
    String CIRCLE_TWO = "Inside circle 2";
    String CIRCLE_THREE = "Inside circle 3";
    String CIRCLE_FOUR = "Inside circle 4";
    String CIRCLE_FIVE = "Inside circle 5";
    String CIRCLE_SIX = "Inside circle 6";
    int OUTSIDE_CIRCLE = 10;
    int INSIDE_CIRCLE = 5;
    int FIRST_CIRCLE = 0;
    int SECOND_CIRCLE = 1;
    int THIRD_CIRCLE = 2;
    int FOURTH_CIRCLE = 3;
    int FIFTH_CIRCLE = 4;
    int SIXTH_CIRCLE = 5;
    int OUTSIDE_ALL = 7;


}
