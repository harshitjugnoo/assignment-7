package com.harshit.circlegame.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.harshit.circlegame.R;
import com.harshit.circlegame.utils.AppConstants;

/**
 * Created by Harshit on 2/11/2015.
 */
public class SecondLevel extends Activity implements View.OnTouchListener, AppConstants {
    private TextView position;
    RelativeLayout playArea; // This is the area on the screen on which OnTouchListener is applied
    float currentPositionX, currentPositionY; // The current position in pixels is stored in these variables
    float[][] centres = new float[6][2]; // This array stores the centers of all the circles
    float commonRadius; // This is the radius common for each circle
    int currentCircle = OUTSIDE_ALL; //For storing the current circle, initialized to a default value of OUTSIDE
    String[] color = new String[6]; //For storing the present color of all the circles
    float[] distances = new float[6]; // This array stores the distance of current position from the centers of all the circles
    ImageButton circles[] = new ImageButton[6]; // Am array of Image Buttons


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_level);
        playArea = (RelativeLayout) findViewById(R.id.play_area);
        playArea.setOnTouchListener(this);
        initializeVariables();

    }

    private void initializeVariables() {
        position = (TextView) findViewById(R.id.position);

        for (int m = 0; m < playArea.getChildCount(); m++) { // This loop initializes all the child views of the 'playArea' relative layout
            circles[m] = (ImageButton) (playArea.getChildAt(m));
        }
        circles[0].post(new Runnable() { // Just to calculate the common radius of all the circles
            @Override
            public void run() {
                commonRadius = circles[0].getWidth() / 2;
            }
        });
        for (int i = 0; i < 6; i++) { //Initially all circles have RED color, so we initialize their color states to RED
            color[i] = RED;
        }
    }

    private void saveCentres() { // This is meant to save the co-ordinates of the center of all circles in a 2-D array called 'centres'

        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 2; j++) {
                if (j == 0) {
                    centres[i][j] = circles[i].getX() + commonRadius;
                } else {
                    centres[i][j] = circles[i].getY() + commonRadius;
                }
            }
        }
    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {
        saveCentres();
        currentPositionX = event.getX(); //The current pixel position is stored here
        currentPositionY = event.getY();
        calculateDistances();

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: // When the cursor is only touching the screen and not moving
                for (int i = 0; i < 6; i++) { // Checks which circle is the one which has distance lesser than the radius from the point of touch
                    if (distances[i] < commonRadius) {
                        position.setText(INSIDE_CIRCLE_X + (i + 1)); //'position' is a TextView which displays the touched circle position
                        currentCircle = i;
                        changeColorOfCircle(currentCircle); //change the color of the current circle
                        break;
                    } else { // If it is not inside any of the circles, then write OUTSIDE in the 'position' TextView
                        position.setText(OUTSIDE);
                        currentCircle = OUTSIDE_ALL;
                    }
                }
                return true;
            case MotionEvent.ACTION_MOVE: //When the cursor is moving on the screen
                int newCircle = circleTouched(); // 'circleTouched' function returns the number of circle which is touched
                if (newCircle < 6) {
                    position.setText(INSIDE_CIRCLE_X + (newCircle + 1)); // 'position' is a TextView which displays the touched circle position
                    if (newCircle != currentCircle) {
                        currentCircle = newCircle;
                        changeBackgroundOnMove(currentCircle); //change the color of the current circle
                    }
                } else {
                    position.setText(OUTSIDE);
                    if (currentCircle < 6) { // If the pointer has come after touching any of the circles
                        if (color[currentCircle].equals(RED)) { // Change the 'color[]' of the last touched circle
                            color[currentCircle] = GREEN;// Coz 'currentCircle' stores the number of the last touched circle
                        } else {
                            color[currentCircle] = RED;
                        }
                    }
                    currentCircle = OUTSIDE_ALL;
                }

                break;

            case MotionEvent.ACTION_UP:
                break;

        }

        return false;
    }

    private int circleTouched() {// This function returns the number of the circle touched
        int num = OUTSIDE_ALL;
        float minDis = Float.MAX_VALUE;
        for (int i = 0; i < 6; i++) {
            if (distances[i] < minDis && distances[i] < commonRadius) {
                minDis = distances[i];
                num = i;
            }
        }
        return num;
    }

    private void changeBackgroundOnMove(int currentCircle) {// This function changes the color of the current circle
        // But does not change the state of the circle in the 'color[]' array
        if (color[currentCircle].equals(RED)) {
            circles[currentCircle].setBackgroundResource(R.drawable.circle_green);
        } else {
            circles[currentCircle].setBackgroundResource(R.drawable.circle_red);
        }

    }

    private void changeColorOfCircle(int currentCircle) { // This function changes the color of the current circle
        // the array 'color' stores the state of each of the circle after the change
        if (color[currentCircle].equals(RED)) { // If the color is RED, make it GREEN
            circles[currentCircle].setBackgroundResource(R.drawable.circle_green);
            color[currentCircle] = GREEN;
        } else {// If the color is GREEN, make it RED
            circles[currentCircle].setBackgroundResource(R.drawable.circle_red);
            color[currentCircle] = RED;
        }
    }

    private void calculateDistances() { // This function calculates the distance of each of the centres from the current point of contact
        //The 'distances[]' array stores the distances from the six centres
        for (int i = 0; i < 6; i++) {
            distances[i] = (float) Math.sqrt(Math.pow(centres[i][0] - currentPositionX, 2) + Math.pow(centres[i][1] - currentPositionY, 2));
        }
    }
}
