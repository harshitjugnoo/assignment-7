package com.harshit.circlegame.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.harshit.circlegame.R;
import com.harshit.circlegame.utils.AppConstants;


public class MainActivity extends Activity implements View.OnTouchListener, AppConstants, View.OnClickListener {

    private TextView position;
    private RelativeLayout playArea;
    private View circle;
    private int positionCode = OUTSIDE_CIRCLE;
    private String color = RED; // This is meant to store the state of the circle
    private Button nextLevel; // This button when clicked takes user to another level
    double centerOfCircleX, centerOfCircleY;
    double abscissa, ordinate, result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        position = (TextView) findViewById(R.id.position);
        playArea = (RelativeLayout) findViewById(R.id.play_area);
        circle = findViewById(R.id.circle);
        nextLevel = (Button) findViewById(R.id.next_level);
        playArea.setOnTouchListener(this);
        nextLevel.setOnClickListener(this);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {

        calculateCenter(); // This function calculates the center of the circle
        double radius = circle.getWidth() / 2; //The radius is calculated
        abscissa = event.getX(); //The current X and Y coordinates
        ordinate = event.getY(); // are stored in abscissa and ordinate
        calculateDistance(); // This function calculates the distance of the center from the current point of contact

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (result < radius) {
                    position.setText(INSIDE);
                    changeColorOnTouch(); // If it is inside, Change the color of the circle
                } else if (result == radius) {
                    position.setText(ON);
                } else {
                    position.setText(OUTSIDE);
                }
                return true;
            case MotionEvent.ACTION_MOVE:
                if (result < radius) {
                    position.setText(INSIDE);
                    changeColorOnMove(); // If it is moving inside, Change the color of the circle but not the color code
                    positionCode = INSIDE_CIRCLE;
                } else if (result == radius) {
                    position.setText(ON);
                } else { //When it is outside the circle
                    position.setText(OUTSIDE);
                    if (positionCode == INSIDE_CIRCLE) {
                        changeColorCode();
                    }
                }
                break;
            case MotionEvent.ACTION_UP:
                break;
        }
        return false;
    }

    private void changeColorCode() {
        if (color.equals(GREEN)) {
            color = RED;
        } else {
            color = GREEN;
        }
        positionCode = OUTSIDE_CIRCLE;
    }

    private void changeColorOnMove() {
        if (color.equals(GREEN)) {
            circle.setBackgroundResource(R.drawable.circle_red);

        } else if (color.equals(RED)) {
            circle.setBackgroundResource(R.drawable.circle_green);
        }
    }

    private void changeColorOnTouch() {
        if (color.equals(RED)) {
            circle.setBackgroundResource(R.drawable.circle_green);
            color = GREEN;

        } else if(color.equals(GREEN)){
            circle.setBackgroundResource(R.drawable.circle_red);
            color = RED;
        }
    }

    private void calculateDistance() {
        double distanceX = Math.pow(centerOfCircleX - abscissa, 2);
        double distanceY = Math.pow(centerOfCircleY - ordinate, 2);
        result = Math.sqrt(distanceX + distanceY); // This is the distance between the current position of the cursor and the center of the circle
    }

    private void calculateCenter() {
        centerOfCircleX = playArea.getWidth() / 2;
        centerOfCircleY = playArea.getHeight() / 2;
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(MainActivity.this, SecondLevel.class);
        startActivity(intent);
    }
}
